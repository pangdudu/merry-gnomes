=begin
  Ok, this will be a super awesome file radar.
  
  use in another class like this:
  
  #event handler
  def file_radar_event info; puts "Fired!"; end;
  
  #add the file_radar 
  def add_file_radar
    fr = FileRadar.new; fr.add_listener self; fr.watch "/tmp/spy"
  end
  
=end

class FileRadar
  
  attr_accessor :frequency
  attr_accessor :stop
  
  def initialize
    #we store the watchlist in here
    @watchlist = {}
    @listeners = []
    @frequency = 10.0
    @fraction = 1.0/@frequency #our sec fraction
    @last = Time.now
    @stop = false
    radarloop
  end
  
  #add a listener
  def add_listener listener
    #check if the method is implemented
    impcheck = defined? listener.file_radar_event 
    #if it is add to listeners
    @listeners << listener if impcheck.eql? "method"
    #if not inform dev
    unless impcheck.eql? "method" 
      puts "Method 'file_radar_event hash' needs to be implemented! Example:"
      puts "def file_radar_event info\n  puts \"Fired!\"\nend"  
    end
  end
  
  #inform listeners
  def inform_listeners info
    @listeners.each {|l| l.file_radar_event info}
  end  
  
  #updates watchlist files
  def update
    @watchlist.each do |name,info|
      alive = File.exists? name
      #status change?
      info[:change] = alive != info[:alive]
      #still alive?
      info[:alive] = alive
      #still there?
      if info[:alive]  
       info[:lastseen] = Time.now
      end
      #delta time since last seen
      info[:delta] = (Time.now - info[:lastseen]) unless info[:lastseen].nil?
      #do something if something changed
      inform_listeners info if info[:change]
    end
  end
  
  #add a new file to the watchlist
  def watch filename
    watched = {}
    watched[:filename] = filename
    watched[:change] = false
    watched[:alive] = false
    #if file you want to watch exists
    if File.exists? filename
      watched[:alive] = true
      watched[:lastseen] = Time.now
    end
    @watchlist[filename] = watched
  end
  
  #the loop
  def radarloop
    waittime = @fraction - (Time.now - @last)
    Thread.new do
      loop do 
        break if @stop
        #wait the correct time
        waittime = @fraction - (Time.now - @last)
        sleep waittime if waittime > 0
        sleep @fraction if waittime < 0
        @last = Time.now
        #do the loop action
        begin
          update #update file watcher
        rescue
          #puts "oops!"
        end
      end
    end
  end
end
