require 'logger'

#little merry logger module
module MerryLogger
  
  attr_accessor :debugname,:merrylogger
  
  #check if there already is a logger
  def logger_check
    if @merrylogger.nil?
      nameless = @debugname.nil? #only used to inform the user
      #to stop output from getting messy, we use a logger
      @merrylogger = Logger.new(STDOUT)
      @merrylogger.level = Logger::DEBUG
      @debugname = "MerryLogger" if nameless
      wlog "No @debugname chosen, please do this!" if nameless
    end
  end
  #error message
  def elog text 
    logger_check #check if logger is setup
    @merrylogger.error "#{debugname}: #{text.to_s}"
  end
  #warning
  def wlog text
    logger_check #check if logger is setup
    @merrylogger.warn "#{debugname}: #{text.to_s}"
  end
  #info message
  def ilog text
    logger_check #check if logger is setup
    @merrylogger.info "#{debugname}: #{text.to_s}"
  end
  #debug message
  def dlog text
    logger_check #check if logger is setup
    @merrylogger.debug "#{debugname}: #{text.to_s}"
  end
end

#finally we just include ourselves, a little dirty ... what the hell :)
include MerryLogger
